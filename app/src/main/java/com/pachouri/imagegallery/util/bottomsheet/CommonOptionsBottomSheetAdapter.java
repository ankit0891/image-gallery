package com.pachouri.imagegallery.util.bottomsheet;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pachouri.imagegallery.R;
import com.pachouri.imagegallery.util.imageloader.AppImageView;
import com.pachouri.imagegallery.util.imageloader.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ankit Pachouri on 23/04/19.
 * <p>
 * Adapter: Common adapter to show options in the bottom sheet
 */
public class CommonOptionsBottomSheetAdapter extends RecyclerView.Adapter<CommonOptionsBottomSheetAdapter.OptionsViewHolder> {

    private List<CommonOptionsBottomSheet> mList;
    private IBottomSheetInteraction mListener;

    public CommonOptionsBottomSheetAdapter(IBottomSheetInteraction listener) {
        mList = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public OptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_common_options_bottom_sheet, parent, false);
        return new OptionsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(OptionsViewHolder holder, int position) {
        CommonOptionsBottomSheet options = mList.get(position);
        ImageLoader.getInstance().load(holder.imageViewOption, options.getResourceId());
        holder.textViewOptionTitle.setText(options.getTitle());
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    /**
     * Update list of options
     *
     * @param list
     */
    public void updateList(List<CommonOptionsBottomSheet> list) {
        this.mList.clear();
        this.mList.addAll(list);
        notifyDataSetChanged();
    }

    public class OptionsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageViewOption)
        protected AppImageView imageViewOption;

        @BindView(R.id.textViewOptionTitle)
        protected TextView textViewOptionTitle;

        public OptionsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> {
                if (mListener != null) {
                    mListener.onOptionClicked(mList.get(getLayoutPosition()));
                }
            });
        }
    }

    /**
     * Interface: Listener to get the option clicked
     */
    public interface IBottomSheetInteraction {
        void onOptionClicked(CommonOptionsBottomSheet option);
    }
}