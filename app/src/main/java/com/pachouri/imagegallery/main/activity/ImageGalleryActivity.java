package com.pachouri.imagegallery.main.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.pachouri.imagegallery.R;
import com.pachouri.imagegallery.main.fragment.ImagesListFragment;

public class ImageGalleryActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        attachNewFragment(new ImagesListFragment());
    }

    /**
     * Method to attach fragment to activity
     *
     * @param fragment
     */
    private void attachNewFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }
}
