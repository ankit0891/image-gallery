package com.pachouri.imagegallery.util;

/**
 * Created by Ankit Pachouri on 22/04/19.
 * <p>
 * Purpose of this class is to keep all the constants at a single place
 */
public class AppConstants {

    //Directory key of Firebase storage and database
    public static final String FIREBASE_DB_KEY = "imageUploads";

    //Intent Keys
    public static final String KEY_INTENT_IMAGE_URI = "intent_image_uri";
}
