package com.pachouri.imagegallery.util.imageloader;

import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

/**
 * Created by Ankit Pachouri on 22/04/19.
 * <p>
 * Image loading methods with different parameters
 */
public interface AppImageLoader<I extends ImageView> {

    //Load image with url, place holder drawable and error drawable
    void load(I imageView, String url, @DrawableRes int placeholderResId, @DrawableRes int errorResId);

    //Load image with url without any placeholder
    void load(I imageView, String url);

    //Load image with uri, place holder drawable and error drawable
    void load(I imageView, Uri uri, @DrawableRes int placeholderResId, @DrawableRes int errorResId);

    //Load image with uri without any placeholder
    void load(I imageView, Uri uri);

    //Load image with drawable
    void load(I imageView, @DrawableRes int resourceId);
}