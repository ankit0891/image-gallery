package com.pachouri.imagegallery.util.diffutil;

import android.support.v7.util.DiffUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ankit Pachouri on 23/04/19.
 * <p>
 * RecyclerView's DiffUtil with Generic type, simply implement GenericInterface for the type of List you wish to compare.
 */
public class DiffCallBack<T extends GenericInterface> extends DiffUtil.Callback {

    private final List<T> mOldList;
    private final List<T> mNewList;

    public DiffCallBack(List<T> oldList, List<T> newList) {
        mOldList = new ArrayList<>();
        mNewList = new ArrayList<>();
        if (oldList != null && !oldList.isEmpty()) {
            mOldList.addAll(oldList);
        }
        if (newList != null && !newList.isEmpty()) {
            mNewList.addAll(newList);
        }
    }

    @Override
    public int getOldListSize() {
        return mOldList != null ? mOldList.size() : 0;
    }

    @Override
    public int getNewListSize() {
        return mNewList != null ? mNewList.size() : 0;
    }

    @Override
    public boolean areItemsTheSame(int oldPosition, int newPosition) {
        return mOldList.get(oldPosition).getObjectId().equals(mNewList.get(newPosition).getObjectId());
    }

    @Override
    public boolean areContentsTheSame(int oldPosition, int newPosition) {
        return mOldList.get(oldPosition).contentEquals(mNewList.get(newPosition));
    }
}