package com.pachouri.imagegallery.util.filepicker;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;

import com.theartofdev.edmodo.cropper.CropImageView;

/**
 * Created by Ankit Pachouri on 23/04/19.
 * <p>
 * Interface: To write methods required to pick the file/image
 */
public interface AppFilePicker {

    /**
     * Pick image
     *
     * @param fragment
     */
    void pickImage(Fragment fragment);

    /**
     * Take a photo
     *
     * @param fragment
     */
    void takePhoto(Fragment fragment);

    /**
     * Crop image
     *
     * @param fragment
     * @param imageUri
     * @param shape
     */
    void cropImageFragment(Fragment fragment, Uri imageUri, CropImageView.CropShape shape);

    /**
     * Handle result once the file is picked. This needs to be placed in your onActivityResult.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     * @param fragment
     * @param filePickListener
     */
    void handleResult(int requestCode, int resultCode, Intent data, Fragment fragment, IFilePickListener filePickListener);

    /**
     * Interface: listener to get the picked file
     */
    interface IFilePickListener {

        /**
         * Called once the file is picked.
         *
         * @param uri
         */
        void onImagePicked(Uri uri);
    }
}