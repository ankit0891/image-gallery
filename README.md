Hi Reader,

First of all thanks for taking time to read this file :)

**Application Name** - Images Gallery

**Language Used** - Java

**Description** - This is a native android application which saves the images on Firebase Database with title and date of uploading the image. Some of the features are:

1. The user can upload the image from gallery or can click and upload.
2. Provision to crop or rotate the image right before uploading the image.
3. Upload image with a title and default time.
4. If user does not have any image in the gallery, the empty view comes up.
5. While viewing the gallery user can toggle the view type as Grid or List. 
6. Bottom sheet has been used to show the options to upload the image.
7. Realtime database and live data in the application updates the ui at the same time. (Image addition or removal are reflected at the same point of time).

**Database** - Firebase Database (Real time)
Please read here for more information - https://firebase.google.com/docs/storage/android/start

Steps to run/install the project:

There are 2 ways to get this project build on any laptop:  One is by cloning the project and the other is direct download.

1. To clone project:
	* Open terminal and move to the desired directory where you wish to keep the project.
		"For an instance, lets assume you wish to keep the project in your Home folder. So simply type cd Home/ and press enter. You are now in the directory."
   	* Now write the git clone command along with the remote url.
   	   **Command** - git clone https://bitbucket.org/ankit0891/image-gallery.git
   	* The project will start cloning. It shows the progress and displays done when its completed.
   	* Check your directory you have your project available.

2. If you do not wish to clone the project.
	* Go to this url and click Download Repository - https://bitbucket.org/ankit0891/image-gallery/downloads/
	* Go to your downloads folder and unzip the file.

Now we have the project in our respective directory. Let's move forward.

3. Open Android Studio.
4. Click on File -> Open
5. Choose the project.
6. The project will start building and once the gradle build gets finished, you can simply click the Run (Play icon) button on top to run the project.

Some troubleshoots, if the project does not build.

1. Try clean build and build project again.
2. Check the build tools version is installed or not on your machine.

Some additional information about the project-

1. The project follows the MVVM Architecture.
2. New features can be easily tweaked in within the project as have kept most of the code as generics and more reusable.
3. Some 3rd party libraries have been used, like "glide" to load images, "event bus" to broadcast events, "butterknife" to inject views, etc.


Some assumptions -

1. Have kept the database and storage rules as disabled for now as no authentication is being implemented to use this application. But in future to restrict the users and have the authenticated users of the application Firebase Auth can be implemented.

Would love to give a temporary access to have a look at the wonderful Firebase console with realtime database updates. :)

Hope you would like it. Please feel free to give the suggestions.

Thanks & Regards,

**Ankit Pachouri**

Email - ankitpachouri0891@gmail.com

Github profile - https://github.com/Pachouri-Sikandar

Portfolio - http://ankitpachouri.online/