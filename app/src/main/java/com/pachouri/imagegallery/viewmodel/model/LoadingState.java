package com.pachouri.imagegallery.viewmodel.model;

/**
 * Created by Ankit Pachouri on 22/04/19.
 * <p>
 * Different Loading States
 */
public enum LoadingState {

    LOADING,
    LOADING_COMPLETED,
    LOADING_FAILED;

    private String mError;

    LoadingState() {
        mError = "";
    }

    public LoadingState withError(String error) {
        mError = error;
        return this;
    }
}
