package com.pachouri.imagegallery.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

/**
 * Created by Ankit Pachouri on 22/04/19.
 * <p>
 * Add the utility or common methods, variables etc. in this class to be used throughout the app
 */
public class CommonUtils {

    /**
     * Method to show toast
     *
     * @param context
     * @param message
     */
    public static void showToast(Context context, String message) {
        if (null != context) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Method to check internet connection available or not
     *
     * @param context
     * @return
     */
    public static boolean isInternetConnected(Context context) {
        if (null != context) {
            try {
                ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context
                        .CONNECTIVITY_SERVICE);
                return connManager.getActiveNetworkInfo() != null
                        && connManager.getActiveNetworkInfo().isAvailable()
                        && connManager.getActiveNetworkInfo().isConnected();
            } catch (Exception ex) {
                return false;
            }
        }
        return false;
    }

    /**
     * Method to convert dp to pixel
     *
     * @param context
     * @param dp
     * @return
     */
    public static float convertDpToPixel(Context context, float dp) {
        if (null != context) {
            float density = context.getResources().getDisplayMetrics().density;
            float pixel = dp * density;
            return pixel;
        }
        return dp;
    }

    /**
     * Method to forcefully hide keyboard
     *
     * @param context
     * @param view
     */
    public static void hideKeyboard(Context context, View view) {
        try {
            if (context != null && view != null) {
                InputMethodManager info = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                info.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
    }
}
