package com.pachouri.imagegallery.util.filepicker;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Ankit Pachouri on 23/04/19.
 * <p>
 * Class to write all the useful methods or variables required to file picker
 */
public class FileManager {

    public static Uri mUri = null;

    public static File createTemporaryImageFile(Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mUri = FileProvider.getUriForFile(context, context.getPackageName(), image);
        return image;
    }

    public static Uri getUri() {
        return mUri;
    }
}
