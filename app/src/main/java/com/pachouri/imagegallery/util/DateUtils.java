package com.pachouri.imagegallery.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ankit Pachouri on 23/04/19.
 */
public class DateUtils {

    private static volatile DateUtils mInstance;
    public static final String DATE_FORMAT_REGULAR_WITH_TIME = "dd MMMM YY, hh:mm a";

    public static synchronized DateUtils getInstance() {
        if (mInstance == null) {
            synchronized (DateUtils.class) {
                if (mInstance == null) {
                    mInstance = new DateUtils();
                }
            }
        }
        return mInstance;
    }

    /**
     * Convert date to any expected date format in String
     *
     * @param date
     * @param expectedDateFormat
     * @return
     */
    public String getDateInRequiredFormat(Date date, String expectedDateFormat) {
        String inputDate = "";
        if (date != null) {
            SimpleDateFormat requiredFormat = new SimpleDateFormat(expectedDateFormat);
            inputDate = requiredFormat.format(date);
        }
        return inputDate;
    }
}