package com.pachouri.imagegallery.util.filepicker;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.pachouri.imagegallery.R;
import com.pachouri.imagegallery.util.CommonUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Ankit Pachouri on 23/04/19.
 * <p>
 * Image Picker which handles all the operations of image picking and handling the result directly or after cropping the image
 */
public class ImagePicker implements AppFilePicker {

    public static final int REQUEST_IMAGE_CAPTURE = 1001;
    public static final int REQUEST_PICK_MEDIA = 1002;

    @Override
    public void pickImage(final Fragment fragment) {
        Dexter.withActivity(fragment.getActivity())
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            String[] mimeTypes = new String[]{"image/*"};
                            StringBuilder mimeTypesStr = new StringBuilder();
                            for (String mimeType : mimeTypes) {
                                mimeTypesStr.append(mimeType).append("|");
                            }
                            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
                            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                            fragment.startActivityForResult(intent, REQUEST_PICK_MEDIA);
                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            CommonUtils.showToast(fragment.getActivity(), fragment.getActivity().getString(R.string.permission_denied));
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                        permissionToken.continuePermissionRequest();
                    }
                }).check();
    }

    @Override
    public void takePhoto(final Fragment fragment) {
        Dexter.withActivity(fragment.getActivity())
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (takePictureIntent.resolveActivity(fragment.getActivity().getPackageManager()) != null) {
                                File photoFile = null;
                                try {
                                    photoFile = FileManager.createTemporaryImageFile(fragment.getContext());
                                } catch (IOException ex) {
                                }
                                if (photoFile != null) {
                                    Uri photoUri = FileManager.getUri();
                                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                                    fragment.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                                }
                            }
                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            CommonUtils.showToast(fragment.getActivity(), fragment.getActivity().getString(R.string.permission_denied));
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken permissionToken) {
                        permissionToken.continuePermissionRequest();
                    }
                }).check();
    }

    @Override
    public void cropImageFragment(Fragment fragment, Uri imageUri, CropImageView.CropShape shape) {
        CropImage.activity(imageUri)
                .setAutoZoomEnabled(true)
                .setRotationDegrees(90)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setCropShape(shape)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .start(fragment.getActivity(), fragment);
    }

    @Override
    public void handleResult(int requestCode, int resultCode, Intent data, Fragment fragment, IFilePickListener filePickListener) {
        switch (requestCode) {
            case REQUEST_PICK_MEDIA:
                if (data != null) {
                    Uri uri = data.getData();
                    showCropOrRotateConfirmationDialog(fragment, (dialog, which) -> {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            cropImageFragment(fragment, uri, CropImageView.CropShape.RECTANGLE);
                        } else {
                            filePickListener.onImagePicked(uri);
                        }
                        dialog.dismiss();
                    });
                } else {
                    CommonUtils.showToast(fragment.getActivity(), fragment.getActivity().getString(R.string.error_something_wrong));
                }
                break;
            case REQUEST_IMAGE_CAPTURE:
                showCropOrRotateConfirmationDialog(fragment, (dialog, which) -> {
                    if (which == DialogInterface.BUTTON_POSITIVE) {
                        cropImageFragment(fragment, FileManager.getUri(), CropImageView.CropShape.RECTANGLE);
                    } else {
                        filePickListener.onImagePicked(FileManager.getUri());
                    }
                    dialog.dismiss();
                });
                break;

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (result != null && result.getUri() != null) {
                    Uri resultUri = result.getUri();
                    filePickListener.onImagePicked(resultUri);
                } else {
                    CommonUtils.showToast(fragment.getActivity(), fragment.getActivity().getString(R.string.error_something_wrong));
                }
                break;
            default: // Empty Default
                break;
        }
    }

    /**
     * Method to show dialog asking user to move forward to crop or rotate the image before uploading
     *
     * @param fragment
     * @param dialogInterface
     */
    private void showCropOrRotateConfirmationDialog(Fragment fragment, DialogInterface.OnClickListener dialogInterface) {
        AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getActivity());
        builder.setTitle(fragment.getActivity().getString(R.string.dialog_crop_title))
                .setMessage(fragment.getActivity().getString(R.string.dialog_crop_message))
                .setCancelable(false)
                .setPositiveButton(fragment.getActivity().getString(R.string.button_yes),
                        (dialog, id) -> dialogInterface.onClick(dialog, id))
                .setNegativeButton(fragment.getActivity().getString(R.string.button_no),
                        (dialog, id) -> dialogInterface.onClick(dialog, id));
        AlertDialog alert = builder.create();
        alert.show();
    }
}