package com.pachouri.imagegallery.viewmodel;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;

import com.pachouri.imagegallery.viewmodel.factory.AppViewModelFactory;
import com.pachouri.imagegallery.viewmodel.provider.UploadImageViewModel;

/**
 * Created by Ankit Pachouri on 22/04/19.
 * <p>
 * ViewModelProvider singleton
 * Class which holds all the view models definitions
 */
public class ViewModelProvider {

    private static volatile ViewModelProvider mInstance;

    public static synchronized ViewModelProvider getInstance() {
        if (mInstance == null) {
            synchronized (ViewModelProvider.class) {
                if (mInstance == null) {
                    mInstance = new ViewModelProvider();
                }
            }
        }
        return mInstance;
    }


    public UploadImageViewModel getUploadImageViewModel(Fragment fragment, String key) {
        AppViewModelFactory factory = AppViewModelFactory.getInstance(fragment, key);
        return ViewModelProviders.of(fragment, factory).get(UploadImageViewModel.class.getName(), UploadImageViewModel.class);
    }
}
