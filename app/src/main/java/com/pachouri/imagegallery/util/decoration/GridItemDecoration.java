package com.pachouri.imagegallery.util.decoration;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.pachouri.imagegallery.util.CommonUtils;

/**
 * Created by Ankit Pachouri on 22/04/19.
 */
public class GridItemDecoration extends RecyclerView.ItemDecoration {

    private int mSpanCount;
    private int mSpacing;

    public GridItemDecoration(Context context, int spanCount, float spacing) {
        mSpanCount = spanCount;
        mSpacing = (int) CommonUtils.convertDpToPixel(context, spacing);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        int column = position % mSpanCount; // item column

        outRect.left = mSpacing - column * mSpacing / mSpanCount;
        outRect.right = (column + 1) * mSpacing / mSpanCount;

        if (position < mSpanCount) { // top edge
            outRect.top = mSpacing;
        }
        outRect.bottom = mSpacing; // item bottom
    }
}
