package com.pachouri.imagegallery.viewmodel.factory;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.pachouri.imagegallery.viewmodel.provider.UploadImageViewModel;

/**
 * Created by Ankit Pachouri on 22/04/19.
 * <p>
 * ViewModelFactory to get the type of ViewModel
 */
public class AppViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private Application mApplication;
    private String mAnyStringData; //Any important parameter required to be passed as an argument to the view model

    private AppViewModelFactory(Fragment fragment) {
        mApplication = fragment.getActivity().getApplication();
    }

    public static AppViewModelFactory getInstance(Fragment fragment, String anyString) {
        AppViewModelFactory factory = new AppViewModelFactory(fragment);
        factory.mAnyStringData = anyString;
        return factory;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(UploadImageViewModel.class)) {
            if (mAnyStringData == null) {
                throw new IllegalArgumentException("You need to pass directory key!");
            }
            return (T) new UploadImageViewModel(mApplication, mAnyStringData);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}