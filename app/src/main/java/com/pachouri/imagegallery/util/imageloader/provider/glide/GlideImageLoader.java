package com.pachouri.imagegallery.util.imageloader.provider.glide;

import android.net.Uri;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pachouri.imagegallery.util.imageloader.AppImageLoader;
import com.pachouri.imagegallery.util.imageloader.AppImageView;

/**
 * Created by Ankit Pachouri on 22/04/19.
 * <p>
 * Glide image loading library which implements AppImageLoader to define different ways (methods) to load images by Glide
 */
public class GlideImageLoader implements AppImageLoader<AppImageView> {

    private static int TIME_OUT_MS = 10000;

    @Override
    public void load(AppImageView imageView, String url, int placeholderResId, int errorResId) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.placeholder(placeholderResId).error(errorResId).timeout(TIME_OUT_MS);
        Glide.with(imageView).setDefaultRequestOptions(requestOptions).load(url).into(imageView);
    }

    @Override
    public void load(AppImageView imageView, String url) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.timeout(TIME_OUT_MS);
        Glide.with(imageView).setDefaultRequestOptions(requestOptions).load(url).into(imageView);
    }

    @Override
    public void load(AppImageView imageView, Uri uri, int placeholderResId, int errorResId) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.placeholder(placeholderResId).error(errorResId).timeout(TIME_OUT_MS);
        Glide.with(imageView).setDefaultRequestOptions(requestOptions).load(uri).into(imageView);
    }

    @Override
    public void load(AppImageView imageView, Uri uri) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.timeout(TIME_OUT_MS);
        Glide.with(imageView).setDefaultRequestOptions(requestOptions).load(uri).into(imageView);
    }

    @Override
    public void load(AppImageView imageView, int resourceId) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.timeout(TIME_OUT_MS);
        Glide.with(imageView).setDefaultRequestOptions(requestOptions).load(resourceId).into(imageView);
    }
}