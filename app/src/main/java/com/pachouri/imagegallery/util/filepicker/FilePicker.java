package com.pachouri.imagegallery.util.filepicker;

/**
 * Created by Ankit Pachouri on 23/04/19.
 * <p>
 * File Picker: In case we wish to change the file picker simply return the instance of the same
 */
public class FilePicker {
    public static AppFilePicker getInstance() {
        return new ImagePicker();
    }
}