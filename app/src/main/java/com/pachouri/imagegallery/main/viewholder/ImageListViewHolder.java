package com.pachouri.imagegallery.main.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pachouri.imagegallery.R;
import com.pachouri.imagegallery.main.adapter.ImagesListAdapter;
import com.pachouri.imagegallery.main.model.UploadImageModel;
import com.pachouri.imagegallery.util.imageloader.AppImageView;
import com.pachouri.imagegallery.util.imageloader.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ankit Pachouri on 22/04/19.
 */
public class ImageListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.imageViewItem)
    protected AppImageView mImageViewItem;

    @BindView(R.id.textViewTitle)
    protected TextView mTextViewTitle;

    @BindView(R.id.textViewUploadDate)
    protected TextView mTextViewUploadDate;

    public ImageListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindView(UploadImageModel uploadImageModel) {
        ImageLoader.getInstance().load(mImageViewItem, uploadImageModel.getUrl());
        mTextViewTitle.setText(uploadImageModel.getTitle());
        mTextViewUploadDate.setText(uploadImageModel.getDateUploaded());
    }

    public static ImageListViewHolder create(ViewGroup parent, ImagesListAdapter.ListLayoutType layoutType) {
        View itemView;
        if (layoutType == ImagesListAdapter.ListLayoutType.Grid) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_grid_item_images, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_images, parent, false);
        }
        return new ImageListViewHolder(itemView);
    }
}
