package com.pachouri.imagegallery.util.imageloader;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by Ankit Pachouri on 22/04/19.
 * <p>
 * Purpose of this image view:
 * To use any image view type for Image Loading simply change the parent class
 * no need to make changes in xml files
 */
public class AppImageView extends AppCompatImageView {

    public AppImageView(Context context) {
        super(context);
    }

    public AppImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
