package com.pachouri.imagegallery.main.model;

import com.pachouri.imagegallery.util.diffutil.GenericInterface;

/**
 * Created by Ankit Pachouri on 23/04/19.
 */
public class UploadImageModel implements GenericInterface {

    private String id;
    private String title;
    private String url;
    private String dateUploaded;

    public UploadImageModel() {
    }

    public UploadImageModel(String id, String title, String url, String dateUploaded) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.dateUploaded = dateUploaded;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDateUploaded() {
        return dateUploaded;
    }

    public void setDateUploaded(String dateUploaded) {
        this.dateUploaded = dateUploaded;
    }

    @Override
    public String getObjectId() {
        return getId();
    }

    @Override
    public boolean contentEquals(Object o) {
        return this == o;
    }
}