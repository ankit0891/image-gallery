package com.pachouri.imagegallery.main.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.pachouri.imagegallery.R;
import com.pachouri.imagegallery.util.AppConstants;
import com.pachouri.imagegallery.util.CommonUtils;
import com.pachouri.imagegallery.util.imageloader.AppImageView;
import com.pachouri.imagegallery.util.imageloader.ImageLoader;
import com.pachouri.imagegallery.viewmodel.ViewModelProvider;
import com.pachouri.imagegallery.viewmodel.provider.UploadImageViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ankit Pachouri on 23/04/19.
 */
public class UploadImageDialogFragment extends AppCompatDialogFragment {

    @BindView(R.id.imageView)
    protected AppImageView mImageView;

    @BindView(R.id.editTextTitle)
    protected EditText mEditTextTitle;

    @BindView(R.id.buttonUpload)
    protected Button mButtonUpload;

    @BindView(R.id.progressBar)
    protected ProgressBar mProgressBar;

    private Uri mImageUri;
    private UploadImageViewModel mViewModel;

    public static UploadImageDialogFragment newInstance(Uri uri) {
        UploadImageDialogFragment dialogFragment = new UploadImageDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.KEY_INTENT_IMAGE_URI, uri);
        dialogFragment.setArguments(bundle);
        return dialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_upload_image, container, false);
        ButterKnife.bind(this, view);
        setCancelable(false);
        getBundle();
        setUpDialog();
        setViewModel();
        return view;
    }

    private void getBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mImageUri = bundle.getParcelable(AppConstants.KEY_INTENT_IMAGE_URI);
        }
    }

    private void setUpDialog() {
        if (mImageUri != null) {
            ImageLoader.getInstance().load(mImageView, mImageUri);
            mButtonUpload.setEnabled(false);
            mEditTextTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (mEditTextTitle.getText().toString().trim().length() >= 3) {
                        mButtonUpload.setEnabled(true);
                    } else {
                        mButtonUpload.setEnabled(false);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        } else {
            CommonUtils.showToast(getContext(), getContext().getString(R.string.error_something_wrong));
            dismissAllowingStateLoss();
        }
    }

    @OnClick(R.id.imageViewClose)
    protected void onCloseClicked() {
        dismissAllowingStateLoss();
    }

    @OnClick(R.id.buttonUpload)
    protected void onUploadClicked() {
        CommonUtils.hideKeyboard(getContext(), mEditTextTitle);
        mButtonUpload.setEnabled(false);
        uploadImage();
    }

    private void setViewModel() {
        if (null == mViewModel) {
            mViewModel = ViewModelProvider.getInstance().getUploadImageViewModel(this, AppConstants.FIREBASE_DB_KEY);
        }

        mViewModel.getProgress().observe(this, progress -> {
            mProgressBar.setProgress(progress);
            if (progress == 100) {
                CommonUtils.showToast(getContext(), getContext().getString(R.string.upload_image_success));
                mButtonUpload.setEnabled(true);
                dismissAllowingStateLoss();
            }
        });
    }

    private void uploadImage() {
        if (mViewModel != null)
            mViewModel.uploadImage(mImageUri, mEditTextTitle.getText().toString());
    }
}