package com.pachouri.imagegallery.util.diffutil;

/**
 * Created by Ankit Pachouri on 23/04/19.
 * <p>
 * Interface to be used with Generic classes (if needed). So that id and object comparison can be performed.
 */
public interface GenericInterface {

    String getObjectId();

    boolean contentEquals(Object o);
}
