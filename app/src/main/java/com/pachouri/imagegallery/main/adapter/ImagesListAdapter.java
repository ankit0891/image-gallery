package com.pachouri.imagegallery.main.adapter;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.pachouri.imagegallery.main.model.UploadImageModel;
import com.pachouri.imagegallery.main.viewholder.ImageListViewHolder;
import com.pachouri.imagegallery.util.diffutil.DiffCallBack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ankit Pachouri on 22/04/19.
 */
public class ImagesListAdapter extends RecyclerView.Adapter<ImageListViewHolder> {

    private List<UploadImageModel> mList;
    private ListLayoutType mLayoutType;

    public enum ListLayoutType {
        List, Grid;
    }

    public ImagesListAdapter(ListLayoutType layoutType) {
        mList = new ArrayList<>();
        mLayoutType = layoutType;
    }

    @NonNull
    @Override
    public ImageListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return ImageListViewHolder.create(viewGroup, mLayoutType);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageListViewHolder holder, int position) {
        holder.bindView(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    /**
     * Set the updated list
     *
     * @param newList
     * @return
     */
    public ImagesListAdapter setList(List<UploadImageModel> newList) {
        DiffCallBack<UploadImageModel> diffCallback = new DiffCallBack<>(mList, newList);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        mList.clear();
        mList.addAll(newList);
        diffResult.dispatchUpdatesTo(this);
        return this;
    }

    /**
     * Get the layout type currently set to the adapter
     *
     * @return
     */
    public ListLayoutType getLayoutType() {
        return mLayoutType;
    }

    /**
     * Set the layout type to the adapter
     *
     * @param layoutType
     */
    public void setLayoutType(ListLayoutType layoutType) {
        mLayoutType = layoutType;
    }
}
