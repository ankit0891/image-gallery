package com.pachouri.imagegallery.util.bottomsheet;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.pachouri.imagegallery.R;

import java.io.Serializable;

/**
 * Created by Ankit Pachouri on 23/04/19.
 * <p>
 * Enum for the options
 */
public enum CommonOptionsBottomSheet implements Serializable {

    Camera(R.drawable.ic_option_camera, R.string.option_camera),
    Gallery(R.drawable.ic_option_image, R.string.option_gallery),
    Cancel(R.drawable.ic_option_cancel, R.string.option_cancel);

    @DrawableRes
    private Integer resourceId;

    @StringRes
    private Integer title;

    CommonOptionsBottomSheet(@DrawableRes Integer resourceId, @StringRes Integer title) {
        this.resourceId = resourceId;
        this.title = title;
    }

    public Integer getResourceId() {
        return resourceId;
    }

    public Integer getTitle() {
        return title;
    }
}