package com.pachouri.imagegallery.main.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.pachouri.imagegallery.R;
import com.pachouri.imagegallery.util.bottomsheet.CommonOptionsBottomSheet;
import com.pachouri.imagegallery.util.bottomsheet.CommonOptionsBottomSheetAdapter;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ankit Pachouri on 23/04/19.
 */
public class UploadOptionsBottomSheetFragment extends BottomSheetDialogFragment implements CommonOptionsBottomSheetAdapter.IBottomSheetInteraction {

    @BindView(R.id.recyclerView)
    public RecyclerView mRecyclerView;

    public static UploadOptionsBottomSheetFragment getInstance() {
        UploadOptionsBottomSheetFragment fragment = new UploadOptionsBottomSheetFragment();
        return fragment;
    }

    @Override
    public BottomSheetDialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                hide();
            }
        };
        return bottomSheetDialog;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View contentView = View.inflate(getContext(), R.layout.fragment_upload_options_bottom_sheet, null);
        dialog.setContentView(contentView);
        ButterKnife.bind(this, contentView);
        CommonOptionsBottomSheetAdapter adapter = new CommonOptionsBottomSheetAdapter(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(adapter);
        List<CommonOptionsBottomSheet> options = new ArrayList<>();
        options.add(CommonOptionsBottomSheet.Camera);
        options.add(CommonOptionsBottomSheet.Gallery);
        options.add(CommonOptionsBottomSheet.Cancel);
        adapter.updateList(options);
    }

    /**
     * Cancel the dialog
     */
    public void cancel() {
        if (getDialog() != null) {
            getDialog().cancel();
        }
    }

    @Override
    public void onOptionClicked(CommonOptionsBottomSheet option) {
        EventBus.getDefault().post(option);
        cancel();
    }
}