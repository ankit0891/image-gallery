package com.pachouri.imagegallery.viewmodel.provider;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

/**
 * Created by Ankit Pachouri on 22/04/19.
 * <p>
 * The purpose of this class is to use the View Model lifecycle owner and
 * this view model should be extended by all the view models of the application
 */
public class AppViewModel extends AndroidViewModel implements LifecycleOwner {

    private LifecycleRegistry mRegister;

    public AppViewModel(@NonNull Application application) {
        super(application);
        mRegister = new LifecycleRegistry(this);
        mRegister.markState(Lifecycle.State.INITIALIZED);
        mRegister.markState(Lifecycle.State.CREATED);
        mRegister.markState(Lifecycle.State.STARTED);
    }

    @Override
    @CallSuper
    public void onCleared() {
        mRegister.markState(Lifecycle.State.DESTROYED);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mRegister;
    }
}