package com.pachouri.imagegallery.viewmodel.provider;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.pachouri.imagegallery.main.model.UploadImageModel;
import com.pachouri.imagegallery.util.DateUtils;
import com.pachouri.imagegallery.viewmodel.model.LoadingState;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Ankit Pachouri on 23/04/19.
 */
public class UploadImageViewModel extends AppViewModel {

    private MutableLiveData<LoadingState> mRefreshLoadingState;
    private MutableLiveData<LoadingState> mLoadingState;
    private MutableLiveData<List<UploadImageModel>> mImagesList;
    private MutableLiveData<Integer> mProgress;
    private StorageReference mStorageReference;
    private DatabaseReference mDatabaseReference;

    public UploadImageViewModel(@NonNull Application application, String key) {
        super(application);
        mRefreshLoadingState = new MutableLiveData<>();
        mLoadingState = new MutableLiveData<>();
        mImagesList = new MutableLiveData<>();
        mProgress = new MutableLiveData<>();
        mStorageReference = FirebaseStorage.getInstance().getReference(key);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference(key);
    }

    /**
     * Get loading state
     *
     * @return
     */
    public LiveData<LoadingState> getLoadingState() {
        return mLoadingState;
    }

    public boolean fetchImagesList() {
        if (mRefreshLoadingState.getValue() != LoadingState.LOADING &&
                mLoadingState.getValue() != LoadingState.LOADING) {
            if (mLoadingState.getValue() == LoadingState.LOADING_COMPLETED) {
                getImages(mRefreshLoadingState);
            } else {
                getImages(mLoadingState);
            }
            return true;
        }
        return false;
    }

    /**
     * Get the Images List
     *
     * @return
     */
    public LiveData<List<UploadImageModel>> getImagesList() {
        return mImagesList;
    }

    /**
     * Fetch images list from Firebase database
     */
    public void getImages(MutableLiveData<LoadingState> loadingState) {
        loadingState.setValue(LoadingState.LOADING);
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<UploadImageModel> list = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    UploadImageModel uploadImage = snapshot.getValue(UploadImageModel.class);
                    list.add(uploadImage);
                }
                mImagesList.setValue(list);
                mLoadingState.setValue(LoadingState.LOADING_COMPLETED);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mLoadingState.setValue(LoadingState.LOADING_FAILED.withError(databaseError.getMessage()));
                Log.v("FETCH ERROR", " : Value: " + databaseError.getMessage());
            }
        });
    }

    /**
     * Get upload progress
     *
     * @return
     */
    public LiveData<Integer> getProgress() {
        return mProgress;
    }

    /**
     * Upload image
     *
     * @param uri
     * @param title
     */
    public void uploadImage(Uri uri, String title) {
        String id = System.currentTimeMillis() + ".png";
        StorageReference storageReference = mStorageReference.child(id);
        storageReference.putFile(uri)
                .addOnSuccessListener(taskSnapshot -> {
                    Calendar calendar = Calendar.getInstance();
                    String uploadTime = DateUtils.getInstance().getDateInRequiredFormat(calendar.getTime(), DateUtils.DATE_FORMAT_REGULAR_WITH_TIME);
                    Task<Uri> result = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                    result.addOnSuccessListener(uri1 -> {
                        UploadImageModel uploadImage = new UploadImageModel(id, title, uri1.toString(), uploadTime);
                        String uploadId = mDatabaseReference.push().getKey();
                        mDatabaseReference.child(uploadId).setValue(uploadImage);
                    });
                })
                .addOnFailureListener(e -> {
                    Log.v("UPLOAD", " : FAIL: " + e.getMessage());
                })
                .addOnProgressListener(taskSnapshot -> {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    mProgress.setValue((int) progress);
                });
    }
}
