package com.pachouri.imagegallery.util.decoration;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.pachouri.imagegallery.util.CommonUtils;

/**
 * Created by Ankit Pachouri on 22/04/19.
 */
public class VerticalListItemDecoration extends RecyclerView.ItemDecoration {

    private int mSpace;
    private int mPadding = 0;

    public VerticalListItemDecoration(Context context, Float space, Float padding) {
        mSpace = (int) CommonUtils.convertDpToPixel(context, space);
        mPadding = (int) CommonUtils.convertDpToPixel(context, padding);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);

        if (parent.getChildLayoutPosition(view) == 0) {
            outRect.top = mPadding;
        }

        if (position == parent.getAdapter().getItemCount() - 1) {
            outRect.bottom = mPadding;
        } else {
            outRect.bottom = mSpace;
        }

        outRect.left = mSpace;
        outRect.right = mSpace;
    }
}
