package com.pachouri.imagegallery.main.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pachouri.imagegallery.R;
import com.pachouri.imagegallery.main.adapter.ImagesListAdapter;
import com.pachouri.imagegallery.util.AppConstants;
import com.pachouri.imagegallery.util.CommonUtils;
import com.pachouri.imagegallery.util.bottomsheet.CommonOptionsBottomSheet;
import com.pachouri.imagegallery.util.decoration.GridItemDecoration;
import com.pachouri.imagegallery.util.decoration.VerticalListItemDecoration;
import com.pachouri.imagegallery.util.filepicker.AppFilePicker;
import com.pachouri.imagegallery.util.filepicker.FilePicker;
import com.pachouri.imagegallery.util.filepicker.ImagePicker;
import com.pachouri.imagegallery.viewmodel.ViewModelProvider;
import com.pachouri.imagegallery.viewmodel.model.LoadingState;
import com.pachouri.imagegallery.viewmodel.provider.UploadImageViewModel;
import com.theartofdev.edmodo.cropper.CropImage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ankit Pachouri on 22/04/19.
 */
public class ImagesListFragment extends Fragment {

    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.fabToggle)
    protected FloatingActionButton mFabToggle;

    @BindView(R.id.progressBar)
    protected ProgressBar mProgressBar;

    @BindView(R.id.layoutErrorContainer)
    protected LinearLayout mLayoutErrorContainer;

    @BindView(R.id.textViewErrorTitle)
    protected TextView mTextViewErrorTitle;

    @BindView(R.id.textViewErrorSubTitle)
    protected TextView mTextViewErrorSubTitle;

    @BindView(R.id.layoutEmptyContainer)
    protected LinearLayout mLayoutEmptyContainer;

    private UploadImageViewModel mViewModel;
    private ImagesListAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private GridLayoutManager mGridLayoutManager;
    private VerticalListItemDecoration mListItemDecoration;
    private GridItemDecoration mGridItemDecoration;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_images_list, container, false);
        ButterKnife.bind(this, view);
        setAdapter();
        initialiseViewModel();
        return view;
    }

    @OnClick(R.id.buttonUpload)
    protected void onUploadClicked() {
        UploadOptionsBottomSheetFragment.getInstance().show(getFragmentManager(), "UploadOptions");
    }

    @OnClick(R.id.fabToggle)
    protected void onToggleClicked() {
        toggleList();
    }

    @Override
    public void onStart() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    @Subscribe
    public void uploadOptionsEvent(CommonOptionsBottomSheet options) {
        switch (options) {
            case Camera:
                FilePicker.getInstance().takePhoto(this);
                break;
            case Gallery:
                FilePicker.getInstance().pickImage(this);
                break;
            default://Empty default
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ImagePicker.REQUEST_PICK_MEDIA:
            case ImagePicker.REQUEST_IMAGE_CAPTURE:
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                FilePicker.getInstance().handleResult(requestCode, resultCode, data, ImagesListFragment.this, new AppFilePicker.IFilePickListener() {
                    @Override
                    public void onImagePicked(Uri uri) {
                        UploadImageDialogFragment fragment = UploadImageDialogFragment.newInstance(uri);
                        getActivity().getSupportFragmentManager().beginTransaction().add(fragment, "UPLOAD_IMAGE_DIALOG").commitAllowingStateLoss();
                    }
                });
                break;
            default: //Empty Default
                break;
        }
    }

    /**
     * Initialize view model
     */
    private void initialiseViewModel() {
        if (null == mViewModel) {
            mViewModel = ViewModelProvider.getInstance().getUploadImageViewModel(this, AppConstants.FIREBASE_DB_KEY);
        }
        mViewModel.getImagesList().observe(this, list -> {
            if (mAdapter != null) {
                mAdapter.setList(list);
                showMainLayout(list.size());
            }
        });

        mViewModel.getLoadingState().observe(this, loadingState -> {
            if (loadingState == LoadingState.LOADING_COMPLETED) {
                showMainLayout(mAdapter != null ? mAdapter.getItemCount() : 0);
            } else if (loadingState == LoadingState.LOADING_FAILED) {
                showSomethingWrongLayout();
            } else {
                showProgressBar();
            }
        });

        onTryAgainClicked();
    }

    /**
     * Initialise adapter, layoutManagers and itemDecorators,
     * <p>
     * and setting default ListLayoutType as List
     */
    private void setAdapter() {
        mLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager
                .VERTICAL, false);
        mGridLayoutManager = new GridLayoutManager(getContext(), 2);
        mListItemDecoration = new VerticalListItemDecoration(getContext(), 10f, 10f);
        mGridItemDecoration = new GridItemDecoration(getContext(), 2, 10f);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.addItemDecoration(mListItemDecoration);
        mAdapter = new ImagesListAdapter(ImagesListAdapter.ListLayoutType.List);
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * Set the Linear List
     */
    private void setLinearList() {
        mRecyclerView.removeItemDecoration(mGridItemDecoration);
        mRecyclerView.addItemDecoration(mListItemDecoration);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mAdapter.setLayoutType(ImagesListAdapter.ListLayoutType.List);
    }

    /**
     * Set the Grid List
     */
    private void setGridList() {
        mRecyclerView.removeItemDecoration(mListItemDecoration);
        mRecyclerView.addItemDecoration(mGridItemDecoration);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mAdapter.setLayoutType(ImagesListAdapter.ListLayoutType.Grid);
    }

    /**
     * Toggle the list types
     */
    private void toggleList() {
        int scrollPosition = 0;
        if (mAdapter.getLayoutType() == ImagesListAdapter.ListLayoutType.Grid) {
            mFabToggle.setImageResource(R.drawable.ic_toggle_grid);
            scrollPosition = mGridLayoutManager.findFirstCompletelyVisibleItemPosition();
            setLinearList();
        } else {
            mFabToggle.setImageResource(R.drawable.ic_toggle_list);
            scrollPosition = mLinearLayoutManager.findFirstCompletelyVisibleItemPosition();
            setGridList();
        }
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    /**** Empty And Error Layout Methods *****/

    @OnClick(R.id.buttonTryAgain)
    protected void onTryAgainClicked() {
        if (CommonUtils.isInternetConnected(getContext())) {
            if (mViewModel != null) mViewModel.fetchImagesList();
        } else {
            showNoInternetLayout();
        }
    }

    /**
     * Method to show only progress bar
     */
    private void showProgressBar() {
        if (getActivity() != null && null != mProgressBar) {
            mProgressBar.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mLayoutErrorContainer.setVisibility(View.GONE);
            mLayoutEmptyContainer.setVisibility(View.GONE);
            mFabToggle.setVisibility(View.GONE);
        }
    }

    /**
     * Method to show content layout
     */
    private void showMainLayout(int size) {
        if (getActivity() != null && null != mProgressBar) {
            mProgressBar.setVisibility(View.GONE);
            mLayoutErrorContainer.setVisibility(View.GONE);
            if (size > 0) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mFabToggle.setVisibility(View.VISIBLE);
                mLayoutEmptyContainer.setVisibility(View.GONE);
            } else {
                mRecyclerView.setVisibility(View.GONE);
                mFabToggle.setVisibility(View.GONE);
                mLayoutEmptyContainer.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Method to show no internet available layout
     */
    private void showNoInternetLayout() {
        if (getActivity() != null && null != mProgressBar) {
            hideOtherViews();
            mTextViewErrorTitle.setText(getString(R.string.error_oops));
            mTextViewErrorSubTitle.setText(getString(R.string.error_message_internet_not_found));
        }
    }

    /**
     * Method to show something went wrong layout
     */
    private void showSomethingWrongLayout() {
        if (getActivity() != null && null != mProgressBar) {
            hideOtherViews();
            mTextViewErrorTitle.setText(getString(R.string.error_sorry));
            mTextViewErrorSubTitle.setText(getString(R.string.error_message_something_wrong));
        }
    }

    /**
     * Hide all other views and show error layout
     */
    private void hideOtherViews() {
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mLayoutEmptyContainer.setVisibility(View.GONE);
        mFabToggle.setVisibility(View.GONE);
        mLayoutErrorContainer.setVisibility(View.VISIBLE);
    }
}