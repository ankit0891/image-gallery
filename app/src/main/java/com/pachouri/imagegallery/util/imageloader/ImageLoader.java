package com.pachouri.imagegallery.util.imageloader;

import com.pachouri.imagegallery.util.imageloader.provider.glide.GlideImageLoader;

/**
 * Created by Ankit Pachouri on 22/04/19.
 * <p>
 * ImageLoader class which returns the instance of GlideImageLoader.
 * Purpose of this class is: in case if we wish to change the image loading library simply return the instance of the same
 */
public class ImageLoader {

    private static AppImageLoader<AppImageView> mInstance;

    public static AppImageLoader<AppImageView> getInstance() {
        if (mInstance == null) {
            mInstance = new GlideImageLoader();
        }
        return mInstance;
    }
}
